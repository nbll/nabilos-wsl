#!/bin/bash
#
# nabilos-wsl
# this is a very minimal build
# for programmmming and mini [linux [server]] on windows or on metal

sudo apt update && sudo apt upgrade

        mkdir -p "$HOME/.local/bin"
        mkdir -p "$HOME/.local/builds"
        mkdir -p "$HOME/.local/builds/programs"
        mkdir -p "$HOME/.local/builds/personal-repos"
        mkdir -p "$HOME/.local/share/bash"
        mkdir -p "$HOME/.local/share/zsh"
        mkdir -p "$HOME/.local/share/fonts"
        mkdir -p "$HOME/.config"
        mkdir -p "$HOME/dls"
        mkdir -p "$HOME/dls/programs"
        mkdir -p "$HOME/docs"
        mkdir -p "$HOME/music"
        mkdir -p "$HOME/pics"
        mkdir -p "$HOME/vids"
        mkdir -p "$HOME/work"
        mkdir -p "$HOME/cs"


        # dotfiles
        echo "alias config='/usr/bin/git --git-dir=$HOME/.local/builds/personal-repos/nbll-dotfiles.git/ --work-tree=$HOME'" >> "$HOME/.bashrc"
        echo "$HOME/.local/builds/personal-repos/" >> .gitignore
        git clone --bare "https://gitlab.com/nbll/nbll-dotfiles.git" "$HOME/.local/builds/personal-repos/nbll-dotfiles.git"
        function config {
           /usr/bin/git --git-dir="$HOME/.local/builds/personal-repos/nbll-dotfiles.git/" --work-tree="$HOME" "$@"
        }
        cd || return
        mkdir -p .config-backup
        # config checkout
        # if [ "$?" = 0 ]; then
        if config checkout; then
            echo "Checked out config.";
        else
            echo "Backing up pre-existing dot files.";
            config checkout 2>&1 | grep -E "\s+\." | awk "{'print $1'}" | xargs -I{} mv {} .config-backup/{}
        fi;
        config checkout
        config config status.showUntrackedFiles no



# installing the list of packages
        sudo xargs apt install -y < package-list.txt
        # xargs -a package-list.txt sudo apt install -y


# zsh highlighting
        git clone "https://github.com/zdharma-continuum/fast-syntax-highlighting" "$HOME/.local/builds/programs/fast-syntax-highlighting/"



        # sc-im
        sudo apt install -y bison libncurses5-dev libncursesw5-dev libxml2-dev libzip-dev pkg-config
        git clone "https://github.com/jmcnamara/libxlsxwriter.git" "$HOME/.local/builds/programs/libxlsxwriter/"
        cd "$HOME/.local/builds/programs/libxlsxwriter/" || return
        make
        sudo make install
        sudo ldconfig
        cd ..
        git clone "https://github.com/andmarti1424/sc-im.git" "$HOME/.local/builds/programs/sc-im/"
        cd "$HOME/.local/builds/programs/sc-im/src/" || return
        make
        sudo make install
# # ./sc-im

        chsh -s /bin/zsh
